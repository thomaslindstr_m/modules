# object-has-property

hasOwnProperty check

```
npm install @amphibian/object-has-property
```

```javascript
var objectHasProperty = require('@amphibian/object-has-property');

objectHasProperty({fish: true}, 'fish'); // > true
objectHasProperty({apples: true}, 'fish'); // > false
```
