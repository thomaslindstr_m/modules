// ---------------------------------------------------------------------------
//  test.js
// ---------------------------------------------------------------------------

    var objectHasProperty = require('./index.js');
    var assert = require('assert');

    describe('object-has-property', function () {
        it('should return true when a property is present', function () {
            assert.equal(true, objectHasProperty({
                fish: true
            }, 'fish'));
        });

        it('should return false when a property is not present', function () {
            assert.equal(false, objectHasProperty({
                fish: true
            }, 'cake'));
        });

        it('should return false for object prototype properties', function () {
            assert.equal(false, objectHasProperty({}, 'hasOwnProperty'));
        });
    });
