# is-number

number type checker

```
npm install @amphibian/is-number
```

```javascript
var isNumber = require('@amphibian/is-number');

isNumber(1); // > true
isNumber('hello'); // > false
```
