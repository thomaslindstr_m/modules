// ---------------------------------------------------------------------------
//  is-object
//
//  object type checker
//  - variable: Variable to be checked
// ---------------------------------------------------------------------------

    var isArray = require('@amphibian/is-array');
    var STRING_OBJECT = 'object';

    function isObject(variable) {
        return (variable !== null)
            && (!isArray(variable))
            && (typeof variable === STRING_OBJECT);
    }

    module.exports = isObject;
