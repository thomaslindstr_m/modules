# iterate-down-array

[![build status](https://gitlab.com/thomaslindstr_m/iterate-down-array/badges/master/build.svg)](https://gitlab.com/thomaslindstr_m/iterate-down-array/commits/master)

while loop abstraction for looping through indices from Array.length to 0

```
npm install @amphibian/iterate-down-array
```

```javascript
var iterateDownArray = require('@amphibian/iterate-down');
var array = ['zero', 'one', 'two'];

// Iterate from the last index to the first
iterateDownArray(array, function (index, i) {
    console.log(index, i); // two 2, one 1, zero 0
});

// Iterate from the last index to the first, but stop if the key is `one`
var question = iterateDownArray(array, function (index, i, end) {
    console.log(index, i); // two 2, one 1

    if (index === 'one') {
        end('hola que hora es');
    }
});

console.log(question); // > hola que hora es
```
