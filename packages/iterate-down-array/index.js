// ---------------------------------------------------------------------------
//  iterate-down-array
//  while loop abstraction for looping through indices from Array.length to 0
// ---------------------------------------------------------------------------

    var iterateDown = require('@amphibian/iterate-down');

    /**
     * Iterate down array
     * @param {array} array
     * @param {function} callback
    **/
    function iterateDownArray(array, callback) {
        return iterateDown(array.length, function (i, end) {
            callback(array[i], i, end);
        });
    }

    module.exports = iterateDownArray;
