// ---------------------------------------------------------------------------
//  is-regexp
//
//  regexp type checker
//  - variable: Variable to be checked
// ---------------------------------------------------------------------------

    var toString = Object.prototype.toString;
    var TAG_REGEXP = '[object RegExp]';

    function isRegExp(variable) {
        return toString.call(variable) === TAG_REGEXP;
    };

    module.exports = isRegExp;
