# is-regexp

[![build status](https://gitlab.com/ci/projects/107900/status.png?ref=master)](https://gitlab.com/ci/projects/107900?ref=master)

regexp type checker

```
npm install @amphibian/is-regexp
```

```javascript
var isRegExp = require('@amphibian/is-regexp');

isRegExp('test'); // > false
isRegExp(/bazinga/); // > true
isRegExp(new RegExp('bazinga')); // > true
```
