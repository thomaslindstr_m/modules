// ---------------------------------------------------------------------------
//  test.js
// ---------------------------------------------------------------------------

    var isRegExp = require('./index.js');
    var assert = require('assert');

    describe('is-regexp', function() {
        it('should return true for regular expressions', function () {
            assert.equal(isRegExp(/bazinga/), true);
        });

        it('should return true for constructed regular expressions', function () {
            assert.equal(isRegExp(new RegExp('bazinga')), true);
        });

        it('should return false for arrays', function () {
            assert.equal(isRegExp([]), false);
        });

        it('should return false for strings', function () {
            assert.equal(isRegExp(''), false);
        });

        it('should return false for functions', function () {
            assert.equal(isRegExp(function () {}), false);
        });

        it('should return false for objects', function () {
            assert.equal(isRegExp({}), false);
        });

        it('should return false for undefined', function () {
            assert.equal(isRegExp(undefined), false);
        });

        it('should return false for null', function () {
            assert.equal(isRegExp(null), false);
        });

        it('should return false for false', function () {
            assert.equal(isRegExp(false), false);
        });

        it('should return false for true', function () {
            assert.equal(isRegExp(true), false);
        });
    });
