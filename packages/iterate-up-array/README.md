# iterate-up-array

[![build status](https://gitlab.com/thomaslindstr_m/iterate-up-array/badges/master/build.svg)](https://gitlab.com/thomaslindstr_m/iterate-up-array/commits/master)

while loop abstraction for looping up through indices from 0 to Array.length

```
npm install @amphibian/iterate-up-array
```

```javascript
var iterateUpArray = require('@amphibian/iterate-up-array');
var array = ['zero', 'one', 'two'];

// Iterate from the first index to the last
iterateUpArray(array, function (index, i) {
    console.log(index, i); // zero 0, one 1, two 2
});

// Iterate from the first index to the last, but stop if the key is `one`
var question = iterateUpArray(array, function (index, i, end) {
    console.log(index, i); // zero 0, one 1

    if (index === 'one') {
        end('hola que hora es');
    }
});

console.log(question); // > hola que hora es
```
