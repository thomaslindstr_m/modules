// ---------------------------------------------------------------------------
//  iterate-up-array
//  while loop abstraction for looping through indices from 0 to Array.length
// ---------------------------------------------------------------------------

    var iterateUp = require('@amphibian/iterate-up');

    /**
     * Iterate up array
     * @param {array} array
     * @param {function} callback
    **/
    function iterateUpArray(array, callback) {
        return iterateUp(array.length, function (i, end) {
            callback(array[i], i, end);
        });
    }

    module.exports = iterateUpArray;
