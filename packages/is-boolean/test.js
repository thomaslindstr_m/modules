// ---------------------------------------------------------------------------
//  test.js
// ---------------------------------------------------------------------------

    var isBoolean = require('./index.js');
    var assert = require('assert');

    describe('is-boolean', function () {
        it('should return true for false', function () {
            assert.equal(isBoolean(false), true);
        });

        it('should return true for true', function () {
            assert.equal(isBoolean(true), true);
        });

        it('should return false for empty strings', function () {
            assert.equal(isBoolean(''), false);
        });

        it('should return false for strings with content', function () {
            assert.equal(isBoolean('hello'), false);
        });

        it('should return false for numbers', function () {
            assert.equal(isBoolean(1), false);
        });

        it('should return false for negative numbers', function () {
            assert.equal(isBoolean(-1), false);
        });

        it('should return false for 0', function () {
            assert.equal(isBoolean(0), false);
        });

        it('should return false for objects', function () {
            assert.equal(isBoolean({}), false);
        });

        it('should return false for functions', function () {
            assert.equal(isBoolean(function () {}), false);
        });

        it('should return false for arrays', function () {
            assert.equal(isBoolean([]), false);
        });

        it('should return false for undefined', function () {
            assert.equal(isBoolean(undefined), false);
        });

        it('should return false for null', function () {
            assert.equal(isBoolean(null), false);
        });
    });
