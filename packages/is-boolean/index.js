// ---------------------------------------------------------------------------
//  is-boolean.js
//
//  boolean type checker
//  - variable: Variable to be checked
// ---------------------------------------------------------------------------

    var toString = Object.prototype.toString;
    var TAG_BOOLEAN = '[object Boolean]';

    function isBoolean(variable) {
        return toString.call(variable) === TAG_BOOLEAN;
    }

    module.exports = isBoolean;
