// ---------------------------------------------------------------------------
//  test.js
// ---------------------------------------------------------------------------

    var iterateUp = require('./index.js');
    var assert = require('assert');

    describe('iterate-up', function() {
        it('should iterate 0 times', function () {
            var i = 0;

            iterateUp(0, function () {
                i++;
            });

            assert.equal(0, i);
        });

        it('should iterate 1 time from 0', function () {
            var i = 0;

            iterateUp(1, function () {
                i++;
            });

            assert.equal(1, i);
        });

        it('should iterate 5 times from 0', function () {
            var i = 0;

            iterateUp(5, function () {
                i++;
            });

            assert.equal(5, i);
        });
    });
