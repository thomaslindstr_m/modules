// ---------------------------------------------------------------------------
//  iterate-up
//  while loop abstraction for iterating up with +1 as increment
// ---------------------------------------------------------------------------

    module.exports = require('@amphibian/iterate')(+1);
