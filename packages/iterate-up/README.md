# iterate-up

[![build status](https://gitlab.com/thomaslindstr_m/iterate-up/badges/master/build.svg)](https://gitlab.com/thomaslindstr_m/iterate-up/commits/master)

while loop abstraction for iterating up with +1 as increment

```
npm install @amphibian/iterate-up
```

```javascript
var iterateUp = require('@amphibian/iterate-up');

// Iterate 5 times
iterateUp(5, function (i) {
    console.log(i); // 0, 1, 2, 3, 4
});

// Iterate 5 times, but stop at 3
var question = iterateUp(5, function (i, end) {
    if (i === 3) {
        end('hola que tal');
    }

    console.log(i); // 0, 1, 2, 3
});

console.log(question); // > hola que hora es
```
