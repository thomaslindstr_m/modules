// ---------------------------------------------------------------------------
//  test.js
// ---------------------------------------------------------------------------

    var encodeFormData = require('./index.js');
    var assert = require('assert');

    describe('encode-form-data', function() {
        it('should successfully encode form data', function () {
            assert.equal(encodeFormData('this: is just a test & stuff'), 'this%3A+is+just+a+test+%26+stuff');
        });
    });
