# encode-form-data

[![build status](https://gitlab.com/ci/projects/31607/status.png?ref=master)](https://gitlab.com/ci/projects/31607?ref=master)

prepare form data for submission

```
npm install @amphibian/encode-form-data
```

```javascript
var encodeFormData = require('@amphibian/encode-form-data');
encodeFormData('this: is just a test & stuff'); // > this%3A+is+just+a+test+%26+stuff
```
