// ---------------------------------------------------------------------------
//  encode-form-data
//
//  prepare form data for submission
// ---------------------------------------------------------------------------

    var spaceRegExp = /%20/g;
    var STRING_PLUS = '+';

    function encodeFormData(string) {
        return encodeURIComponent(string).replace(spaceRegExp, STRING_PLUS);
    }

    module.exports = encodeFormData;
