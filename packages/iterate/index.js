// ---------------------------------------------------------------------------
//  iterate
//  functional while loop abstraction to return iterators on an increment
// ---------------------------------------------------------------------------

    var ERROR_INVALID_INCREMENT = 'iterate: invalid increment';
    var STRING_SPACE = ' ';
    var ZERO = 0;
    var PLUS_ONE = +1;

    function iterate(increment) {
        return function iterateOnIncrement(length, callback) {
            var _continue = true;
            var _return;

            var end = function () {
                _continue = false;

                if (arguments.length === PLUS_ONE) {
                    _return = arguments[ZERO];
                } else {
                    _return = arguments;
                }
            };

            if (increment > ZERO) {
                var _i = ZERO; while (_continue && _i < length) {
                    callback(_i, end);
                _i += increment; }
            } else if (increment < ZERO) {
                var _i = length; while (_continue && _i) {
                    callback(_i - PLUS_ONE, end);
                _i += increment; }
            } else {
                throw new Error(ERROR_INVALID_INCREMENT + STRING_SPACE + increment);
            }

            return _return;
        };
    }

    module.exports = iterate;
