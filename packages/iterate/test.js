// ---------------------------------------------------------------------------
//  test.js
// ---------------------------------------------------------------------------

    var iterate = require('./index.js');
    var assert = require('assert');

    describe('iterate', function () {
        it('should be able to re-use iterators', function () {
            var iterateUp = iterate(+1);

            var i = 0;

            iterateUp(5, function (j, end) {
                if (j === 1) {
                    end();
                }

                i++;
            });

            iterateUp(5, function () {
                i++;
            });

            assert.equal(i, 7);
        });

        it('should iterate 5 times from 0', function () {
            var iterateUp = iterate(+1);
            var i = 0;

            iterateUp(5, function () {
                i++;
            });

            assert.equal(5, i);
        });

        it('should iterate 5 times from 5 to 0', function () {
            var iterateDown = iterate(-1);
            var i = 0;

            iterateDown(5, function () {
                i++;
            });

            assert.equal(5, i);
        });

        it('should iterate on even numbers from 0 to 5', function () {
            var iterateUpEven = iterate(+2);
            var numbers = [];

            iterateUpEven(5, function (i) {
                numbers.push(i);
            });

            assert.deepEqual([0, 2, 4], numbers);
        });

        it('should iterate on even numbers from 0 to 6', function () {
            var iterateUpEven = iterate(+2);
            var i = 0;

            iterateUpEven(6, function () {
                i++;
            });

            assert.equal(3, i);
        });

        it('should iterate on even numbers from 6 to 0', function () {
            var iterateDownEven = iterate(-2);
            var i = 0;

            iterateDownEven(6, function () {
                i++;
            });

            assert.equal(3, i);
        });

        it('should iterate from 0 to 6 but end at 4', function () {
            var iterateUp = iterate(+1);
            var i = 0;

            iterateUp(6, function (index, end) {
                i++;

                if (i === 4) {
                    end();
                }
            });

            assert.equal(4, i);
        });

        it('should let the first iteration index be 0 when iterating up', function () {
            var iterateUp = iterate(+1);
            var first = null;

            iterateUp(5, function (index, end) {
                first = index;
                end();
            });

            assert.equal(0, first);
        });

        it('should let the first iteration index be 4 when iterating down from 5', function () {
            var iterateDown = iterate(-1);
            var first = null;

            iterateDown(5, function (index, end) {
                first = index;
                end();
            });

            assert.equal(4, first);
        });

        it('should be able to return the data passed to the end function', function () {
            var iterateUp = iterate(+1);
            var data = iterateUp(5, function (index, end) {
                end('data');
            });

            assert.equal('data', data);
        });

        it('should be able to return an array of data if more arguments were passed', function () {
            var iterateUp = iterate(+1);
            var data = iterateUp(5, function (index, end) {
                end('data', 'is', 'very', 'cool');
            });

            assert.equal(4, Object.keys(data).length);
        });
    });
