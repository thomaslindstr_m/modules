# iterate

[![build status](https://gitlab.com/thomaslindstr_m/iterate/badges/master/build.svg)](https://gitlab.com/thomaslindstr_m/iterate/commits/master)

functional while loop abstraction to return iterators on an increment

```
npm install @amphibian/iterate
```

```javascript
var iterate = require('@amphibian/iterate');

// Iterate 5 times from 0
var iterateUp = iterate(+1);

iterateUp(5, function (i) {
    console.log(i); // 0, 1, 2, 3, 4
});

// Iterate on even numbers from 0 to 5
var iterateEven = iterate(+2);

iterateEven(5, function (i) {
    console.log(i); // 0, 2, 4
});

// Iterate towards 0 from 5, but end at 3
var iterateDown = iterate(-1);
var greeting = iterateDown(5, function (i, end) {
    console.log(i);

    if (i === 3) {
        end('hola que tal');
    }
});

console.log(greeting); // > hola que tal
```
