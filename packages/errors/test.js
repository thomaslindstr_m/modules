// ---------------------------------------------------------------------------
//  test.js
// ---------------------------------------------------------------------------

    var errors = require('./index.js');
    var assert = require('assert');

    describe('errors', function() {
        it('message get a colon appended when no code is set', () => {
            var _error = errors.new_error('My error')(null, 'test');
            assert.equal(_error.message, 'My error: test');
        });

        it('code get a colon appended when data is set', () => {
            var _error = errors.new_error('My error', {type: 'my_code'})(null, 'test');
            assert.equal(_error.message, 'My error (my_code): test');
        });

        it('separate error data with spaces when more are provided', () => {
            var _error = errors.new_error('My error')(null, 'test', 'something');
            assert.equal(_error.message, 'My error: test something');
        });

        it('missing_required_parameters with data', () => {
            var _error = errors.missing_required_parameters(null, 'email');

            assert.equal(_error.code, 'missing_required_parameters');
            assert.equal(_error.status, 400);
            assert.equal(_error.message, 'Missing Required Parameters (missing_required_parameters): email');
            assert.equal(_error.data[0], 'email');
        });

        it('missing_required_parameters with custom code', () => {
            var _error = errors.missing_required_parameters('missing_email', 'email');

            assert.equal(_error.code, 'missing_email');
            assert.equal(_error.type, 'missing_required_parameters');
            assert.equal(_error.status, 400);
            assert.equal(_error.message, 'Missing Required Parameters (missing_email): email');
            assert.equal(_error.data[0], 'email');
        });

        it('not_found with no data', () => {
            var _error = errors.not_found();

            assert.equal(_error.code, 'not_found');
            assert.equal(_error.status, 404);
            assert.equal(_error.message, 'Not Found (not_found)');
        });

        it('method_not_allowed with no data', () => {
            var _error = errors.method_not_allowed();

            assert.equal(_error.code, 'method_not_allowed');
            assert.equal(_error.status, 405);
            assert.equal(_error.message, 'Method Not Allowed (method_not_allowed)');
        });

        it('missing_required_parameters with no data', () => {
            var _error = errors.missing_required_parameters();

            assert.equal(_error.code, 'missing_required_parameters');
            assert.equal(_error.status, 400);
            assert.equal(_error.message, 'Missing Required Parameters (missing_required_parameters)');
        });

        it('invalid_input with no data', () => {
            var _error = errors.invalid_input();

            assert.equal(_error.code, 'invalid_input');
            assert.equal(_error.status, 400);
            assert.equal(_error.message, 'Invalid Input (invalid_input)');
        });

        it('type_error with no data', () => {
            var _error = errors.type_error();

            assert.equal(_error.code, 'type_error');
            assert.equal(_error.status, 400);
            assert.equal(_error.message, 'Type Error (type_error)');
        });

        it('unauthorized with no data', () => {
            var _error = errors.unauthorized();

            assert.equal(_error.code, 'unauthorized');
            assert.equal(_error.status, 401);
            assert.equal(_error.message, 'Unauthorized (unauthorized)');
        });

        it('rate_limit_exceeded with no data', () => {
            var _error = errors.rate_limit_exceeded();

            assert.equal(_error.code, 'rate_limit_exceeded');
            assert.equal(_error.status, 429);
            assert.equal(_error.message, 'Rate Limit Exceeded (rate_limit_exceeded)');
        });

        it('fatal_error with no data', () => {
            var _error = errors.fatal_error();

            assert.equal(_error.code, 'fatal_error');
            assert.equal(_error.status, 500);
            assert.equal(_error.message, 'Fatal Error (fatal_error)');
        });
    });
