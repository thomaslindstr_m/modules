# errors

[![build status](https://gitlab.com/thomaslindstr_m/errors/badges/master/build.svg)](https://gitlab.com/thomaslindstr_m/errors/commits/master)

easily create meaningful errors

```
npm install @amphibian/errors
```

```javascript
var errors = require('@amphibian/errors');
throw errors.invalid_input('invalid_email_address', 'email', 't@hom.as');
```
