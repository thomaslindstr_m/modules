// ---------------------------------------------------------------------------
//  errors
//  easily create meaningful errors
// ---------------------------------------------------------------------------

    var objectHasProperty = require('@amphibian/object-has-property');

    /**
     * Create a new error
     * @param {string} prefix
     * @param {object} information
     *
     * @returns {function} error creation function
    **/
    function new_error(prefix, information = {}) {
        return (code, ...data) => {
            const _dataLength = data.length;
            const _errorCode = code || information.type;
            const _errorMessageFragments = [];

            if (prefix) {
                if (_errorCode || (_dataLength === 0)) {
                    _errorMessageFragments.push(prefix);
                } else {
                    _errorMessageFragments.push(`${prefix}:`);
                }
            }

            if (_errorCode) {
                if (_dataLength === 0) {
                    _errorMessageFragments.push(`(${_errorCode})`);
                } else {
                    _errorMessageFragments.push(`(${_errorCode}):`);
                }
            }

            if (_dataLength > 0) {
                _errorMessageFragments.push(data.join(' '));
            }

            const _error = new Error(_errorMessageFragments.join(' '));

            _error.status = information.status || 400;
            _error.type = information.type || 'error';
            _error.code = _errorCode;
            _error.data = data || [];

            if (!information.stack) {
                _error.stack = null;
            }

            return _error;
        };
    }

    const errors = {
        new_error: new_error,

        not_found: new_error('Not Found', {
            status: 404,
            type: 'not_found'
        }),
        method_not_allowed: new_error('Method Not Allowed', {
            status: 405,
            type: 'method_not_allowed'
        }),
        missing_required_parameters: new_error('Missing Required Parameters', {
            status: 400,
            type: 'missing_required_parameters'
        }),
        invalid_input: new_error('Invalid Input', {
            status: 400,
            type: 'invalid_input'
        }),
        type_error: new_error('Type Error', {
            status: 400,
            type: 'type_error'
        }),
        unauthorized: new_error('Unauthorized', {
            status: 401,
            type: 'unauthorized'
        }),
        rate_limit_exceeded: new_error('Rate Limit Exceeded', {
            status: 429,
            type: 'rate_limit_exceeded'
        }),
        fatal_error: new_error('Fatal Error', {
            status: 500,
            type: 'fatal_error',
            stack: true
        })
    };

    module.exports = errors;
