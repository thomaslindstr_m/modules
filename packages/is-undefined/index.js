// ---------------------------------------------------------------------------
//  is-undefined.js
//
//  undefined type checker
//  - variable: Variable to be checked
// ---------------------------------------------------------------------------

    var STRING_UNDEFINED = 'undefined';

    function isUndefined(variable) {
        return typeof variable === STRING_UNDEFINED;
    }

    module.exports = isUndefined;
