// ---------------------------------------------------------------------------
//  test.js
// ---------------------------------------------------------------------------

    var isUndefined = require('./index.js');
    var assert = require('assert');

    describe('is-undefined', function () {
        it('should return true for undefined', function () {
            assert.equal(isUndefined(undefined), true);
        });

        it('should return false for false', function () {
            assert.equal(isUndefined(false), false);
        });

        it('should return false for true', function () {
            assert.equal(isUndefined(true), false);
        });

        it('should return false for empty strings', function () {
            assert.equal(isUndefined(''), false);
        });

        it('should return false for strings with content', function () {
            assert.equal(isUndefined('hello'), false);
        });

        it('should return false for numbers', function () {
            assert.equal(isUndefined(1), false);
        });

        it('should return false for negative numbers', function () {
            assert.equal(isUndefined(-1), false);
        });

        it('should return false for 0', function () {
            assert.equal(isUndefined(0), false);
        });

        it('should return false for objects', function () {
            assert.equal(isUndefined({}), false);
        });

        it('should return false for functions', function () {
            assert.equal(isUndefined(function () {}), false);
        });

        it('should return false for arrays', function () {
            assert.equal(isUndefined([]), false);
        });

        it('should return false for null', function () {
            assert.equal(isUndefined(null), false);
        });
    });
