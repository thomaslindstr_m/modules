# is-undefined

undefined type checker

```
npm install @amphibian/is-undefined
```

```javascript
var isUndefined = require('@amphibian/is-undefined');

isUndefined(undefined); // > true
isUndefined('hello'); // > false
```
