# extend

[![build status](https://gitlab.com/ci/projects/107880/status.png?ref=master)](https://gitlab.com/ci/projects/107880?ref=master)

extends a parent object while letting keys from child overwrite it

```
npm install @amphibian/extend
```

```javascript
var extend = require('@amphibian/extend');

var parent = {
    obj: {
        foo: 'bar',
        bar: 'foo'
    },
    obj2: {
        foo: 'bar',
        bar: 'foo'
    },
    arr: [0, 1, 2, 3]
};

var child = {
    obj: {
        foo: 'overwrite'
    },
    obj2: false,
    arr: ['hello']
};

// Extend parent object
extend(parent, child); // > {obj: {foo: 'overwrite', bar: 'foo'}, obj2: false, arr: ['hello']}
```
