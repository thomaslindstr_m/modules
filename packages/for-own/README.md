# for-own

[![build status](https://gitlab.com/ci/projects/107891/status.png?ref=master)](https://gitlab.com/ci/projects/107891?ref=master)

for...in abstraction to iterate over an objects own keys

```
npm install @amphibian/for-own
```

```javascript
var forOwn = require('@amphibian/for-own');
var object = {
    foo: 'bar',
    bar: 'foo'
};

// Iterate over object keys and values
forOwn(object, function (key, value) {
    console.log(key, value);
});

// Iterate over object keys, but stop if the key is `foo`
var question = forOwn(object, function (key, value, end) {
    console.log(key, value);

    if (key === 'foo') {
        end('hola que hora es');
    }
});

console.log(question); // > hola que hora es
```
