# copy
[![build status](https://gitlab.com/ci/projects/104617/status.png?ref=master)](https://gitlab.com/ci/projects/104617?ref=master)

copies variable content

```
npm install @amphibian/copy
```

```javascript
var copy = require('@amphibian/copy');

var object = {
    obj: {
        foo: 'bar',
        bar: 'foo'
    },
    obj2: {
        foo: 'bar',
        bar: 'foo'
    },
    arr: [0, 1, 2, 3]
};

// Copy an object
copy(object);
```
