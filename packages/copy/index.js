// ---------------------------------------------------------------------------
//  copy
//
//  copies variable content
//  - content {Object|Array}
// ---------------------------------------------------------------------------

    var extend = require('@amphibian/extend');
    var isArray = require('@amphibian/is-array');
    var isObject = require('@amphibian/is-object');

    function copy(content) {
        if (isObject(content)) {
            return extend({}, content);
        } else if (isArray(content)) {
            return extend([], content);
        }

        return content;
    }

    module.exports = copy;
