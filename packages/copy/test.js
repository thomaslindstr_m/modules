// ---------------------------------------------------------------------------
//  test.js
// ---------------------------------------------------------------------------

    var copy = require('./index.js');
    var assert = require('assert');

    describe('extend', function () {
        var object; beforeEach(function () {
            object = {
                obj: {
                    foo: 'bar',
                    bar: 'foo'
                },
                obj2: {
                    foo: 'bar',
                    bar: 'foo',
                    obj1: {
                        obj2: {
                            test: 'fish'
                        }
                    }
                },
                arr: [[0, {hello: 'there'}, 2], 1, 2, 3],
                str: 'bar'
            };
        });

        it('should contain all the contents of the object', function () {
            var newObject = copy(object);
            assert.equal(newObject.obj.foo, 'bar');
        });

        it('should not replace keys in the originating object', function () {
            var newObject = copy(object);
            newObject.str = 'bar';

            assert.equal(object.str, 'bar');
        });

        it('should not replace keys deep in the originating object', function () {
            var newObject = copy(object);
            newObject.obj.foo = 'foo';

            assert.equal(object.obj.foo, 'bar');
        });

        it('should copy array keys', function () {
            var newObject = copy(object);
            assert.equal(newObject.arr[0][0], 0);
        });

        it('should copy objects in array', function () {
            var newObject = copy(object);
            assert.equal(newObject.arr[0][1].hello, 'there');
        });

        it('should not replace originating string variables', function () {
            var string = 'hello';
            var string2 = copy(string);

            string2 += '2';
            assert.equal(string, 'hello');
        });

        it('should copy complex objects just as well as JSON.stringify', function () {
            var newObject = copy(object);
            var compareObject = JSON.parse(JSON.stringify(object));

            assert.equal(JSON.stringify(compareObject), JSON.stringify(newObject));
        });

        it('should copy arrays', function () {
            var oldArray = [1, 2];
            var newArray = copy(oldArray);

            newArray.pop();

            assert.equal(oldArray.length, 2);
            assert.equal(newArray.length, 1);
            assert.equal(newArray[0], 1);
        });
    });
