// ---------------------------------------------------------------------------
//  test.js
// ---------------------------------------------------------------------------

    var isString = require('./index.js');
    var assert = require('assert');

    describe('is-string', function() {
        it('should return true for strings', function () {
            assert.equal(isString(''), true);
        });

        it('should return false for objects', function () {
            assert.equal(isString({}), false);
        });

        it('should return false for functions', function () {
            assert.equal(isString(function () {}), false);
        });

        it('should return false for arrays', function () {
            assert.equal(isString([]), false);
        });

        it('should return false for undefined', function () {
            assert.equal(isString(undefined), false);
        });

        it('should return false for null', function () {
            assert.equal(isString(null), false);
        });

        it('should return false for false', function () {
            assert.equal(isString(false), false);
        });

        it('should return false for true', function () {
            assert.equal(isString(true), false);
        });
    });
