// ---------------------------------------------------------------------------
//  is-string
//
//  string type checker
//  - variable: Variable to be checked
// ---------------------------------------------------------------------------

    var STRING_STRING = 'string';

    function isString(variable) {
        return typeof variable === STRING_STRING;
    }

    module.exports = isString;
