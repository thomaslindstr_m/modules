# in-array

[![build status](https://gitlab.com/ci/projects/107899/status.png?ref=master)](https://gitlab.com/ci/projects/107899?ref=master)

boolean check to see if an index is in an array

```
npm install @amphibian/in-array
```

```javascript
var inArray = require('@amphibian/in-array');
var mammals = ['cat', 'dog', 'horse'];

inArray('horse', mammals); // > true
inArray('fish', mammals); // > false
```
