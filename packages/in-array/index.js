// ---------------------------------------------------------------------------
//  in-array
//
//  boolean check to see if an index is in an array
//  - index: The index to find in the array
//  - array {Array}: The array to search in
// ---------------------------------------------------------------------------

    var minusOne = -1;

    function inArray(index, array) {
        return array.indexOf(index) !== minusOne;
    }

    module.exports = inArray;
