// ---------------------------------------------------------------------------
//  test.js
// ---------------------------------------------------------------------------

    var inArray = require('./index.js');
    var assert = require('assert');

    describe('in-array', function() {
        var mammals = ['cat', 'dog', 'horse'];

        it('should return true when the index is in the array', function () {
            assert.equal(inArray('cat', mammals), true);
            assert.equal(inArray('dog', mammals), true);
            assert.equal(inArray('horse', mammals), true);
        });

        it('should return false when the index is not in the array', function () {
            assert.equal(inArray('fish', mammals), false);
            assert.equal(inArray('ca', mammals), false);
        });
    });
