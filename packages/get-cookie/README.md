# get-cookie

[![build status](https://gitlab.com/thomaslindstr_m/get-cookie/badges/master/build.svg)](https://gitlab.com/thomaslindstr_m/get-cookie/commits/master)

get browser cookie

```
npm install @amphibian/get-cookie
```

```javascript
var getCookie = require('@amphibian/get-cookie');
console.log(getCookie('my_cookie')); // > my_cookie_value
```
