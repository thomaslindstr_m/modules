// ---------------------------------------------------------------------------
//  index.js
// ---------------------------------------------------------------------------

    var iterateUpArray = require('@amphibian/iterate-up-array');

    var cookieSeparatorRegExp = /;\s?/;
    var STRING_EQUALS = '=';

    /**
     * Get cookie
     * @param {string} name
     *
     * @returns {string} value
    **/
    function getCookie(name) {
        var _cookies = window.document.cookie.split(cookieSeparatorRegExp);
        var _match = iterateUpArray(_cookies, function (cookie, i, end) {
            var _pair = cookie.split(STRING_EQUALS);

            if (_pair[0] === name) {
                end(window.decodeURIComponent(_pair[1]));
            }
        });

        return _match || false;
    }

    module.exports = getCookie;
