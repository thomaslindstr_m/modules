// ---------------------------------------------------------------------------
//  test.js
// ---------------------------------------------------------------------------

    var isArray = require('./index.js');
    var assert = require('assert');

    describe('is-array', function() {
        it('should return true for arrays', function () {
            assert.equal(isArray([]), true);
        });

        it('should return false for strings', function () {
            assert.equal(isArray(''), false);
        });

        it('should return false for functions', function () {
            assert.equal(isArray(function () {}), false);
        });

        it('should return false for objects', function () {
            assert.equal(isArray({}), false);
        });

        it('should return false for undefined', function () {
            assert.equal(isArray(undefined), false);
        });

        it('should return false for null', function () {
            assert.equal(isArray(null), false);
        });

        it('should return false for false', function () {
            assert.equal(isArray(false), false);
        });

        it('should return false for true', function () {
            assert.equal(isArray(true), false);
        });
    });
