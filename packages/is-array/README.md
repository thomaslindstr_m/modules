# is-array

[![build status](https://gitlab.com/ci/projects/107900/status.png?ref=master)](https://gitlab.com/ci/projects/107900?ref=master)

array type checker

```
npm install @amphibian/is-array
```

```javascript
var isArray = require('@amphibian/is-array');

isArray([]); // > true
isArray({}); // > false
```
