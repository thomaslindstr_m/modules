# @amphibian/server

[![build status](https://gitlab.com/thomaslindstr_m/server/badges/master/build.svg)](https://gitlab.com/thomaslindstr_m/server/commits/master) [![coverage report](https://gitlab.com/thomaslindstr_m/server/badges/master/coverage.svg)](https://gitlab.com/thomaslindstr_m/server/commits/master)


A bare bones node.js server that makes setup and maintaining a breeze.

```javascript
import createServer from '@amphibian/server';

const server = createServer();

async function helloWorldHandler(context, next) {
    context.status = 200;
    context.body = {message: 'Hello World!'};
}

server.registerHandler('helloWorldHandler', server.route(helloWorldHandler, {
    method: 'get',
    path: '/hello-world'
}));
```

## Advanced routing

To take advantage of `RegExp` routes using the built in `route` utility, simply send a regular expression instead of a string in the `path` key of the `options` object:

```javascript
async function advancedHandler(context, next) {
    const [message] = context.args;
    context.status = 200;
    context.body = {message: `Your message is: ${message}`};
}

server.registerHandler('advancedHandler', server.route(advancedHandler, {
    method: 'get',
    path: /^\/message\/([^\/]+)$/
}));
```

Any `RegExp` matches are placed as an Array in `context.args`. For reference, the original route (whether it's a `RegExp` or a `String`) is placed in `context.routePath`.
