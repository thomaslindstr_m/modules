// ---------------------------------------------------------------------------
//  app: index.js
// ---------------------------------------------------------------------------

    import EMPTY_STRING from 'empty-string';
    import chalk from 'chalk';
    import Koa from 'koa';

    import * as env from 'app/env';

    import createHandler from 'app/middleware/handler';
    import createRouter from 'app/middleware/route';

    import logMiddleware from 'app/middleware/log';
    import errorMiddleware from 'app/middleware/error';
    import notFoundMiddleware from 'app/middleware/not-found';

    function createApp(options = {}) {
        const app = new Koa();

        const {registerHandler, handlerMiddleware} = createHandler(app);
        const router = createRouter(app);
        const _startTime = Date.now();

        app.on('log', (...data) => {
            if (env.isLoggingEnabled) {
                console.log(...data);
            }
        });

        app.on('error', (error) => {
            app.emit('log',
                chalk.red('[Error]'),
                chalk.grey('›'),
                error.message,
                error.stack || EMPTY_STRING
            );
        });

        app.emit('log', chalk.cyan('›'), chalk.green('Starting up...'));

        app.use(logMiddleware);
        app.use(errorMiddleware);
        app.use(handlerMiddleware);
        app.use(notFoundMiddleware);

        const listener = app.listen(options.port || 3000, () => {
            app.emit('log',
                chalk.cyan('›'),
                chalk.green('Now listening'),
                chalk.grey('at port'),
                chalk.yellow(listener.address().port),
                chalk.grey('after'),
                chalk.magenta(`${Date.now() - _startTime}ms`)
            );
        });

        return {
            env,
            app,
            listener,
            router,
            registerHandler
        };
    }

    export default createApp;
