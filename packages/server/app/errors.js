// ---------------------------------------------------------------------------
//  app: errors.js
// ---------------------------------------------------------------------------

    import chalk from 'chalk';
    import objectHasProperty from '@amphibian/object-has-property';

    /**
     * Create a new error
     * @param {string} prefix
     * @param {object} information
     *
     * @returns {function} error creation function
    **/
    function newError(prefix, information = {}) {
        return (code, ...data) => {
            const _errorCode = code || information.type;
            const _errorMessageFragments = [];
            const _dataLength = data.length;

            if (prefix) {
                if (_errorCode || (_dataLength === 0)) {
                    _errorMessageFragments.push(prefix);
                } else {
                    _errorMessageFragments.push(`${prefix}:`);
                }
            }

            if (_errorCode) {
                if (_dataLength === 0) {
                    _errorMessageFragments.push(`(${_errorCode})`);
                } else {
                    _errorMessageFragments.push(`(${_errorCode}):`);
                }
            }

            if (_dataLength > 0) {
                _errorMessageFragments.push(data.join(' '));
            }

            const _error = new Error(_errorMessageFragments.join(' '));

            _error.status = information.status || 400;
            _error.type = information.type || 'error';
            _error.code = _errorCode;
            _error.data = data || [];

            if (!information.stack) {
                _error.stack = null;
            }

            if (objectHasProperty(information, 'log')
            && (information.log === true)) {
                console.log(
                    chalk.bgRed(' '),
                    chalk.red(_error.type),
                    chalk.red(`(${_error.code})`),
                    _error.message,
                    chalk.grey(JSON.stringify(_error.data, null, 4))
                );
            }

            return _error;
        };
    }

    const errors = {
        not_found: newError('Not Found', {
            status: 404,
            type: 'not_found'
        }),
        method_not_allowed: newError('Method Not Allowed', {
            status: 405,
            type: 'method_not_allowed'
        }),
        missing_required_parameters: newError('Missing Required Parameters', {
            status: 400,
            type: 'missing_required_parameters'
        }),
        type_error: newError('Type Error', {
            status: 400,
            type: 'type_error'
        })
    };

    export default errors;
    export {
        newError,
        errors
    };
