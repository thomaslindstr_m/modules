// ---------------------------------------------------------------------------
//  app: env.js
// ---------------------------------------------------------------------------

    const environment = process.env.NODE_ENV;
    const isDevelopment = environment === 'development';
    const isProduction = environment === 'production';
    const isTest = environment === 'test';

    let isLoggingEnabled = process.env.LOGGING_ENABLED === 'true';
    const setLoggingEnabled = (isEnabled) => (isLoggingEnabled = isEnabled);

    export {
        environment,
        isDevelopment,
        isProduction,
        isTest,

        isLoggingEnabled,
        setLoggingEnabled
    };
