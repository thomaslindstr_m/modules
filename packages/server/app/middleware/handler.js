// ---------------------------------------------------------------------------
//  app/middleware: handler.js
// ---------------------------------------------------------------------------

    import chalk from 'chalk';

    import isString from '@amphibian/is-string';
    import isArray from '@amphibian/is-array';
    import isObject from '@amphibian/is-object';
    import isFunction from '@amphibian/is-function';

    import errors from 'app/errors';

    /**
     * Register handler to use in the app
     * @param {object} app
     * @param {array} handlers
     * @param {string} name
     * @param {function} handler
    **/
    function registerHandler(app, handlers, name, handler) {
        if (!app) {
            throw errors.missing_required_parameters(null, 'app');
        } else if (!isObject(app)) {
            throw errors.type_error(null, 'Object', 'app');
        }

        if (!handlers) {
            throw errors.missing_required_parameters(null, 'handlers');
        } else if (!isArray(handlers)) {
            throw errors.type_error(null, 'Array', 'handlers');
        }

        if (isFunction(name)
        && (!handler)) {
            handler = name;
            name = handler.name || undefined;
        }

        if (name
        && (!isString(name))) {
            throw errors.type_error(null, 'String', 'name');
        }

        if (!handler) {
            throw errors.missing_required_parameters(null, 'handler');
        } else if (!isFunction(handler)) {
            throw errors.type_error(null, 'Function', 'handler');
        }

        handlers.push(handler);
        app.emit(`handlers:did-add`, name || handler.name);
    }

    /**
     * Get the next handler
     * @param {array} handlers
     * @param {number} i
     * @param {object} context
     * @param {function} next
     *
     * @returns {function} next
    **/
    function nextHandler(handlers, i, context, next) {
        const _handler = handlers[i];

        if (_handler) {
            return async () => {
                await _handler(context, await nextHandler(handlers, i + 1, context, next));
            };
        }

        return async () => {
            await next(context);
        };
    }

    /**
     * Handler middleware that sets up registered handlers
     * @param {array} handlers
     * @param {object} context
     * @param {function} next
    **/
    async function handlerMiddleware(handlers, context, next) {
        if (handlers.length > 0) {
            await handlers[0](context, await nextHandler(handlers, 1, context, next));
        } else {
            await next(context);
        }
    }

    /**
     * Create handler
     * @param {object} app
     *
     * @returns {function} handlerMiddleware
    **/
    function createHandler(app) {
        if (!app) {
            throw errors.missing_required_parameters(null, 'app');
        } else if (!isObject(app)) {
            throw errors.type_error(null, 'Object', 'app');
        }

        const _handlers = [];

        app.on(`handlers:did-add`, (name) => {
            app.emit('log',
                chalk.green('[handlerMiddleware]'),
                chalk.grey('›'),
                'Registered handler',
                chalk.blue(name)
            );
        });

        return {
            handlers: _handlers,
            handlerMiddleware: handlerMiddleware.bind(null, _handlers),
            registerHandler: registerHandler.bind(null, app, _handlers)
        };
    }

    export default createHandler;
    export {
        registerHandler,
        handlerMiddleware,
        createHandler
    };
