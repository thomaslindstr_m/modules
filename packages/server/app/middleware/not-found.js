// ---------------------------------------------------------------------------
//  app/middleware: not-found.js
// ---------------------------------------------------------------------------

    import errors from 'app/errors';

    /**
     * Not Found (404) middleware that throws a 404 not found error
    **/
    function notFoundMiddleware() {
        throw errors.not_found();
    }

    export default notFoundMiddleware;
