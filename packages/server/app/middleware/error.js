// ---------------------------------------------------------------------------
//  app/middleware: error.js
// ---------------------------------------------------------------------------

    /**
     * Error middleware that writes JSON errors to the client
     * @param {object} context
     * @param {function} next
    **/
    async function errorMiddleware(context, next) {
        try {
            await next();
        } catch (error) {
            context.status = error.status || 500;
            context.body = {
                error: error.code,
                message: error.message
            };

            if (context.status !== 404) {
                context.app.emit('error', error);
            }
        }
    }

    export default errorMiddleware;
