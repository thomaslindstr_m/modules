// ---------------------------------------------------------------------------
//  app: router.js
// ---------------------------------------------------------------------------

    import chalk from 'chalk';

    import isString from '@amphibian/is-string';
    import isObject from '@amphibian/is-object';
    import isRegExp from '@amphibian/is-regexp';
    import isFunction from '@amphibian/is-function';
    import inArray from '@amphibian/in-array';
    import objectHasProperty from '@amphibian/object-has-property';

    import errors from 'app/errors';

    /**
     * Check if a path matches the target path
     * @param {string} currentPath
     * @param {string|regexp} targetPath
     *
     * @returns {boolean}
    **/
    function doesPathMatch(currentPath, targetPath) {
        if (isString(targetPath) && (currentPath === targetPath)) {
            return true;
        } else if (isRegExp(targetPath) && targetPath.test(currentPath)) {
            return true;
        }

        return false;
    }

    /**
     * Router middleware
     * @param {object} app
     * @param {object} paths
     * @param {function} handler
     * @param {object} options
    **/
    function routeMiddleware(app, paths, handler, options) {
        if (!app) {
            throw errors.missing_required_parameters(null, 'app');
        } else if (!isObject(app)) {
            throw errors.type_error(null, 'Object', 'app');
        }

        if (!paths) {
            throw errors.missing_required_parameters(null, 'paths');
        } else if (!isObject(paths)) {
            throw errors.type_error(null, 'Object', 'paths');
        }

        if (!handler) {
            throw errors.missing_required_parameters(null, 'handler');
        } else if (!isFunction(handler)) {
            throw errors.type_error(null, 'Function', 'handler');
        }

        if (!options) {
            throw errors.missing_required_parameters(null, 'options');
        } else if (!isObject(options)) {
            throw errors.type_error(null, 'Object', 'options');
        }

        if (options.method
        && (!isString(options.method))) {
            throw errors.type_error(null, 'String', 'options.method');
        }

        if (!options.path) {
            throw errors.missing_required_parameters(null, 'options.path');
        }

        if (!isString(options.path)
        && (!isRegExp(options.path))) {
            throw errors.type_error(null, 'String|RegExp', 'options.path');
        }

        const _method = (options.method)
            ? options.method.toUpperCase()
            : 'GET';
        const _path = options.path;
        const _isPathRegExp = isRegExp(_path);

        if (!objectHasProperty(paths, _path)) {
            paths[_path] = [];
        }

        if (!inArray(_method, paths[_path])) {
            paths[_path].push(_method);
        }

        app.emit(`routes:did-add`, _path);

        return async (context, next) => {
            if (doesPathMatch(context.path, _path)) {
                const _allowedMethods = paths[_path];
                context.routePath = _path;

                if (context.method === 'OPTIONS') {
                    context.status = 200;
                    context.body = _allowedMethods.join(', ');

                    return;
                } else if (context.method === _method) {
                    if (_isPathRegExp) {
                        const _matches = context.path.match(_path);
                        _matches.shift();
                        context.args = _matches;
                    }

                    await handler(context, next);
                    return;
                }

                if (!inArray(context.method, _allowedMethods)) {
                    throw errors.method_not_allowed();
                }
            }

            await next(context, next);
        };
    }

    /**
     * Create router
     * @param {object} app
     *
     * @returns {function} routeMiddleware
    **/
    function createRouter(app) {
        if (!app) {
            throw errors.missing_required_parameters(null, 'app');
        } else if (!isObject(app)) {
            throw errors.type_error(null, 'Object', 'app');
        }

        const _paths = {};

        app.on(`routes:did-add`, (path) => {
            app.emit('log',
                chalk.green('[routeMiddleware]'),
                chalk.grey('›'),
                'Registered path',
                chalk.blue(path)
            );
        });

        return routeMiddleware.bind(null, app, _paths);
    }

    export default createRouter;
    export {
        doesPathMatch,
        routeMiddleware,
        createRouter
    };
