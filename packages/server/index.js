// ---------------------------------------------------------------------------
//  index.js
// ---------------------------------------------------------------------------

    var app = null;

    try {
        app = require('./compiled-app');
    } catch (error) {
        require('babel-polyfill');
        require('babel-register');
        app = require('./app');
    }

    module.exports = app;
