// ---------------------------------------------------------------------------
//  test: env.js
// ---------------------------------------------------------------------------

    import test from 'ava';
    import * as env from 'app/env';

    test('environment should be environment', (assert) => {
        assert.is(env.environment, process.env.NODE_ENV);
    });

    test('should allow logging to be activated/deactivated', (assert) => {
        env.setLoggingEnabled(true);
        assert.is(env.isLoggingEnabled, true);
        env.setLoggingEnabled(false);
        assert.is(env.isLoggingEnabled, false);
    });
