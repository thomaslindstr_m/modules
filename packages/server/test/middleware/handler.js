// ---------------------------------------------------------------------------
//  tests/middleware: handler.js
// ---------------------------------------------------------------------------

    import Promise from 'bluebird';
    import Koa from 'koa';
    import test from 'ava';

    import {
        registerHandler as registerHandlerFromScratch,
        createHandler
    } from 'app/middleware/handler';

    test('failing: createHandler with no app', async (assert) => {
        try {
            createHandler();
        } catch (error) {
            assert.is(error.code, 'missing_required_parameters');
            assert.deepEqual(error.data, ['app']);
        }
    });

    test('failing: createHandler with invalid type app', async (assert) => {
        try {
            createHandler([]);
        } catch (error) {
            assert.is(error.code, 'type_error');
            assert.deepEqual(error.data, ['Object', 'app']);
        }
    });

    test('should work with no handler', async (assert) => {
        const {handlerMiddleware} = createHandler(new Koa());
        assert.plan(1);

        await handlerMiddleware({}, async () => {
            assert.pass();
        });
    });

    test('should add handler', async (assert) => {
        const {handlers, registerHandler} = createHandler(new Koa());
        const _handler = async () => {};

        registerHandler(_handler);
        assert.deepEqual(handlers, [_handler]);
    });

    test('should add named handler', async (assert) => {
        const {handlers, registerHandler} = createHandler(new Koa());
        const _handler = async () => {};

        registerHandler('named-handler', _handler);
        assert.deepEqual(handlers, [_handler]);
    });

    test('should call handlers', async (assert) => {
        const {registerHandler, handlerMiddleware} = createHandler(new Koa());
        const _context = {};

        registerHandler(async (context, next) => {
            context.handler = true;
            await next();
        });

        await handlerMiddleware(_context, async (context) => {
            assert.true(context.handler);
        });
    });

    test('should call handlers consecutively', async (assert) => {
        const {registerHandler, handlerMiddleware} = createHandler(new Koa());
        const _context = {order: [0]};

        registerHandler(async (context, next) => {
            context.order.push(1);
            await next();
        });

        registerHandler(async (context, next) => {
            context.order.push(2);
            await next();
        });

        await handlerMiddleware(_context, async (context) => {
            context.order.push(3);
            assert.deepEqual(context.order, [0, 1, 2, 3]);
        });
    });

    test('should await async handlers', async (assert) => {
        const {registerHandler, handlerMiddleware} = createHandler(new Koa());
        const _context = {order: [0]};

        registerHandler(async (context, next) => {
            await new Promise((resolve) => {
                setTimeout(() => {
                    context.order.push(1);
                    resolve();
                });
            });

            context.order.push(2);
            await next();
        });

        registerHandler(async (context, next) => {
            context.order.push(3);
            await next();
        });

        await handlerMiddleware(_context, async (context) => {
            context.order.push(4);
            assert.deepEqual(context.order, [0, 1, 2, 3, 4]);
        });
    });

    test('should stop if next is not called', async (assert) => {
        const {registerHandler, handlerMiddleware} = createHandler(new Koa());
        assert.plan(1);

        registerHandler(async () => {
            await new Promise((resolve) => {
                setTimeout(() => {
                    resolve();
                }, 500);
            });

            assert.pass();
        });

        registerHandler(async () => {
            assert.fail('Next was called');
        });

        await handlerMiddleware({}, async () => {
            assert.fail('Next was called');
        });
    });

    test('failing: registerHandler with no app provided', (assert) => {
        try {
            registerHandlerFromScratch();
        } catch (error) {
            assert.is(error.code, 'missing_required_parameters');
            assert.deepEqual(error.data, ['app']);

            return;
        }

        assert.fail('No error thrown');
    });

    test('failing: registerHandler with invalid type app provided', (assert) => {
        try {
            registerHandlerFromScratch([]);
        } catch (error) {
            assert.is(error.code, 'type_error');
            assert.deepEqual(error.data, ['Object', 'app']);

            return;
        }

        assert.fail('No error thrown');
    });

    test('failing: registerHandler with no handlers provided', (assert) => {
        try {
            registerHandlerFromScratch(new Koa());
        } catch (error) {
            assert.is(error.code, 'missing_required_parameters');
            assert.deepEqual(error.data, ['handlers']);

            return;
        }

        assert.fail('No error thrown');
    });

    test('failing: registerHandler with invalid type handlers provided', (assert) => {
        try {
            registerHandlerFromScratch(new Koa(), {});
        } catch (error) {
            assert.is(error.code, 'type_error');
            assert.deepEqual(error.data, ['Array', 'handlers']);

            return;
        }

        assert.fail('No error thrown');
    });

    test('failing: registerHandler with invalid type name provided', (assert) => {
        try {
            registerHandlerFromScratch(new Koa(), [], {});
        } catch (error) {
            assert.is(error.code, 'type_error');
            assert.deepEqual(error.data, ['String', 'name']);

            return;
        }

        assert.fail('No error thrown');
    });

    test('failing: registerHandler with no handler provided', (assert) => {
        try {
            registerHandlerFromScratch(new Koa(), [], '');
        } catch (error) {
            assert.is(error.code, 'missing_required_parameters');
            assert.deepEqual(error.data, ['handler']);

            return;
        }

        assert.fail('No error thrown');
    });

    test('failing: registerHandler with invalid type handler provided', (assert) => {
        try {
            registerHandlerFromScratch(new Koa(), [], '', {});
        } catch (error) {
            assert.is(error.code, 'type_error');
            assert.deepEqual(error.data, ['Function', 'handler']);

            return;
        }

        assert.fail('No error thrown');
    });
