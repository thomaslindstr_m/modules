// ---------------------------------------------------------------------------
//  tests/middleware: error.js
// ---------------------------------------------------------------------------

    import Koa from 'koa';
    import test from 'ava';
    import errorMiddleware from 'app/middleware/error';

    test('should allow things to happen after it if there are no errors', async (assert) => {
        let _didHappen = false;

        await errorMiddleware({app: new Koa()}, async () => {
            _didHappen = true;
        });

        if (_didHappen) {
            assert.pass();
        } else {
            assert.fail();
        }
    });

    test('should catch and print error messages', async (assert) => {
        const _context = {app: new Koa()};
        const _errorMessage = 'Error message';

        _context.app.on('error', () => {
            // ... do nothing
        });

        await errorMiddleware(_context, async () => {
            throw new Error(_errorMessage);
        });

        assert.is(_context.body.message, _errorMessage);
    });

    test('should print error codes', async (assert) => {
        const _context = {app: new Koa()};
        const _errorCode = 'error';

        _context.app.on('error', () => {
            // ... do nothing
        });

        await errorMiddleware(_context, async () => {
            const _error = new Error();
            _error.code = _errorCode;
            throw _error;
        });

        assert.is(_context.body.error, _errorCode);
    });

    test('should set context status', async (assert) => {
        const _context = {app: new Koa()};
        const _errorStatus = 401;

        _context.app.on('error', () => {
            // ... do nothing
        });

        await errorMiddleware(_context, async () => {
            const _error = new Error();
            _error.status = _errorStatus;
            throw _error;
        });

        assert.is(_context.status, _errorStatus);
    });

    test('should do nothing if there is no error', async (assert) => {
        const _context = {app: new Koa()};
        const _body = 'hello';

        await errorMiddleware(_context, async () => {
            _context.body = _body;
        });

        assert.deepEqual(_context.body, _body);
    });

    test('should emit errors to an app', async (assert) => {
        assert.plan(2);

        const _context = {
            app: {
                emit: (event, error) => {
                    assert.is(event, 'error');
                    assert.is(error.code, 'silly_error');
                }
            }
        };

        await errorMiddleware(_context, async () => {
            const _error = new Error('silly error');
            _error.code = 'silly_error';

            throw _error;
        });
    });

    test('should not emit errors to an app if status is 404', async (assert) => {
        assert.plan(1);

        const _context = {
            app: {
                emit: () => {
                    assert.fail('Error was emit');
                }
            }
        };

        await errorMiddleware(_context, async () => {
            const _error = new Error('silly error');
            _error.code = 'silly_error';
            _error.status = 404;

            throw _error;
        });

        assert.pass();
    });
