// ---------------------------------------------------------------------------
//  tests/middleware: route.js
// ---------------------------------------------------------------------------

    import Koa from 'koa';
    import test from 'ava';

    import {
        doesPathMatch,
        createRouter,
        routeMiddleware
    } from 'app/middleware/route';

    test('string paths should match', (assert) => {
        assert.true(doesPathMatch('/path', '/path'));
    });

    test('regexp path should match', (assert) => {
        assert.true(doesPathMatch('/path/123', /^\/path\/([\d]+)$/));
    });

    test('should bind to paths', async (assert) => {
        const _route = createRouter(new Koa());
        const _context = {method: 'GET', path: '/test'};

        await _route(async (context) => {
            context.routed = true;
        }, {method: 'GET', path: '/test'})(_context, () => {
            assert.fail('Next was called');
        });

        assert.true(_context.routed);
    });

    test('should default method to GET', async (assert) => {
        const _route = createRouter(new Koa());
        const _context = {method: 'GET', path: '/test'};

        await _route(async (context) => {
            context.routed = true;
        }, {path: '/test'})(_context, () => {
            assert.fail('Next was called');
        });

        assert.true(_context.routed);
    });

    test('should set context.routePath', async (assert) => {
        const _route = createRouter(new Koa());
        const _context = {method: 'GET', path: '/test'};

        await _route(async (context, next) => {
            _context.routed = true;
            await next();
        }, {method: 'GET', path: '/test'})(_context, () => {
            _context.nexted = true;
            assert.is(_context.routePath, '/test');
        });

        if (!_context.routed) {
            assert.fail('Not routed.');
        }

        if (!_context.nexted) {
            assert.fail('Not nexted.');
        }
    });

    test('should set RegExp context.routePath', async (assert) => {
        const _route = createRouter(new Koa());
        const _context = {method: 'GET', path: '/test/something'};

        const _routeRegExp = /^\/test\/([^/]+)$/;

        await _route(async (context, next) => {
            _context.routed = true;
            await next();
        }, {method: 'GET', path: _routeRegExp})(_context, () => {
            _context.nexted = true;
            assert.is(_context.routePath, _routeRegExp);
        });

        if (!_context.routed) {
            assert.fail('Not routed.');
        }

        if (!_context.nexted) {
            assert.fail('Not nexted.');
        }
    });

    test('should bind regexp path', async (assert) => {
        const _route = createRouter(new Koa());
        const _context = {method: 'GET', path: '/test/123'};

        await _route(async (context) => {
            context.routed = true;
        }, {method: 'GET', path: /^\/test\/([\d]+)$/})(_context, () => {
            assert.fail('Next was called');
        });

        assert.true(_context.routed);
    });

    test('should pass regexp match to context', async (assert) => {
        const _route = createRouter(new Koa());
        const _context = {method: 'GET', path: '/test/123'};

        await _route(async () => {}, {
            method: 'GET',
            path: /^\/test\/([\d]+)$/
        })(_context, () => {
            assert.fail('Next was called');
        });

        assert.is(JSON.stringify(_context.args), JSON.stringify(['123']));
    });

    test('should pass multiple regexp matches to context', async (assert) => {
        const _route = createRouter(new Koa());
        const _context = {method: 'GET', path: '/test/123/456/789'};

        await _route(async () => {}, {
            method: 'GET',
            path: /^\/test\/([\d]+)\/([\d]+)\/([\d]+)$/
        })(_context, () => {
            assert.fail('Next was called');
        });

        assert.is(JSON.stringify(_context.args), JSON.stringify(['123', '456', '789']));
    });

    test('should leak when path does not match', async (assert) => {
        const _route = createRouter(new Koa());
        const _context = {method: 'GET', path: '/test-2'};

        await _route(async (context) => {
            context.routed = true;
        }, {method: 'GET', path: '/test'})(_context, (context) => {
            assert.is(context.routed, undefined);
        });

        if (_context.routed) {
            assert.fail();
        }
    });

    test('should throw when method does not match', async (assert) => {
        const _route = createRouter(new Koa());
        const _context = {method: 'POST', path: '/test'};

        try {
            await _route(async (context) => {
                context.routed = true;
            }, {method: 'GET', path: '/test'})(_context, () => {});
        } catch (error) {
            assert.is(error.message, 'Method Not Allowed (method_not_allowed)');
            assert.is(error.code, 'method_not_allowed');
            assert.is(error.status, 405);

            return;
        }

        assert.fail('No error thrown');
    });

    test('should not throw when method does not match one route method but matches another', async () => {
        const _route = createRouter(new Koa());
        const _context = {method: 'POST', path: '/test'};

        const _firstRoute = _route(async () => {}, {
            method: 'GET',
            path: '/test'
        });

        _route(async () => {}, {
            method: 'POST',
            path: '/test'
        });

        _firstRoute(_context, async () => {});
    });

    test('should throw when method does not match for multiple routes', async (assert) => {
        const _route = createRouter(new Koa());
        const _context = {method: 'DELETE', path: '/test'};

        try {
            const _firstRoute = _route(async () => {}, {
                method: 'GET',
                path: '/test'
            });

            _route(async () => {}, {
                method: 'POST',
                path: '/test'
            });

            await _firstRoute(_context, () => {});
        } catch (error) {
            assert.is(error.message, 'Method Not Allowed (method_not_allowed)');
            assert.is(error.code, 'method_not_allowed');
            assert.is(error.status, 405);

            return;
        }

        assert.fail('No error thrown');
    });

    test('should automatically resolve OPTIONS request', async (assert) => {
        const _route = createRouter(new Koa());
        const _context = {method: 'OPTIONS', path: '/test'};

        await _route(async (context) => {
            context.routed = true;
        }, {method: 'GET', path: '/test'})(_context, () => {
            assert.fail('Next was called');
        });

        assert.is(_context.body, 'GET');
        assert.is(_context.status, 200);
    });

    test('should resolve OPTIONS request when multiple methods are allowed', async (assert) => {
        const _route = createRouter(new Koa());
        const _context = {method: 'OPTIONS', path: '/test'};

        await _route(async () => {}, {
            method: 'GET',
            path: '/test'
        })(_context, () => {
            assert.fail('Next was called');
        });

        await _route(async () => {}, {
            method: 'POST',
            path: '/test'
        })(_context, () => {
            assert.fail('Next was called');
        });

        assert.is(_context.body, 'GET, POST');
        assert.is(_context.status, 200);
    });

    test('failing: should require an app object', async (assert) => {
        try {
            createRouter();
        } catch (error) {
            assert.is(error.code, 'missing_required_parameters');
            assert.is(error.data[0], 'app');

            return;
        }

        assert.fail('No error thrown');
    });

    test('failing: should not allow invalid type app', async (assert) => {
        try {
            createRouter([]);
        } catch (error) {
            assert.is(error.code, 'type_error');
            assert.is(error.data[0], 'Object');
            assert.is(error.data[1], 'app');

            return;
        }

        assert.fail('No error thrown');
    });

    test('failing: should require an app object on the middleware', async (assert) => {
        try {
            routeMiddleware();
        } catch (error) {
            assert.is(error.code, 'missing_required_parameters');
            assert.is(error.data[0], 'app');

            return;
        }

        assert.fail('No error thrown');
    });

    test('failing: should not allow invalid type app on the middleware', async (assert) => {
        try {
            routeMiddleware([]);
        } catch (error) {
            assert.is(error.code, 'type_error');
            assert.is(error.data[0], 'Object');
            assert.is(error.data[1], 'app');

            return;
        }

        assert.fail('No error thrown');
    });

    test('failing: should require a paths object on the middleware', async (assert) => {
        try {
            routeMiddleware({});
        } catch (error) {
            assert.is(error.code, 'missing_required_parameters');
            assert.is(error.data[0], 'paths');

            return;
        }

        assert.fail('No error thrown');
    });

    test('failing: should not allow invalid type paths on the middleware', async (assert) => {
        try {
            routeMiddleware({}, []);
        } catch (error) {
            assert.is(error.code, 'type_error');
            assert.is(error.data[0], 'Object');
            assert.is(error.data[1], 'paths');

            return;
        }

        assert.fail('No error thrown');
    });

    test('failing: should require a handler', async (assert) => {
        const _route = createRouter(new Koa());

        try {
            await _route();
        } catch (error) {
            assert.is(error.code, 'missing_required_parameters');
            assert.is(error.data[0], 'handler');

            return;
        }

        assert.fail('No error thrown');
    });

    test('failing: should require a handler of type function', async (assert) => {
        const _route = createRouter(new Koa());

        try {
            await _route([]);
        } catch (error) {
            assert.is(error.code, 'type_error');
            assert.is(error.data[0], 'Function');
            assert.is(error.data[1], 'handler');

            return;
        }

        assert.fail('No error thrown');
    });

    test('failing: should require options', async (assert) => {
        const _route = createRouter(new Koa());

        try {
            await _route(() => {});
        } catch (error) {
            assert.is(error.code, 'missing_required_parameters');
            assert.is(error.data[0], 'options');

            return;
        }

        assert.fail('No error thrown');
    });

    test('failing: should require an options object', async (assert) => {
        const _route = createRouter(new Koa());

        try {
            await _route(() => {}, []);
        } catch (error) {
            assert.is(error.code, 'type_error');
            assert.is(error.data[0], 'Object');
            assert.is(error.data[1], 'options');

            return;
        }

        assert.fail('No error thrown');
    });

    test('failing: should not allow methods that are not strings', async (assert) => {
        const _route = createRouter(new Koa());

        try {
            await _route(async () => {}, {
                method: () => {}
            });
        } catch (error) {
            assert.is(error.code, 'type_error');
            assert.is(error.data[0], 'String');
            assert.is(error.data[1], 'options.method');

            return;
        }

        assert.fail('No error thrown');
    });

    test('failing: should require a path', async (assert) => {
        const _route = createRouter(new Koa());

        try {
            await _route(async () => {}, {});
        } catch (error) {
            assert.is(error.code, 'missing_required_parameters');
            assert.is(error.data[0], 'options.path');

            return;
        }

        assert.fail('No error thrown');
    });

    test('failing: should not allow paths that are neither string nor regexp', async (assert) => {
        const _route = createRouter(new Koa());

        try {
            await _route(async () => {}, {
                method: 'POST',
                path: () => {}
            });
        } catch (error) {
            assert.is(error.code, 'type_error');
            assert.is(error.data[0], 'String|RegExp');
            assert.is(error.data[1], 'options.path');

            return;
        }

        assert.fail('No error thrown');
    });
