// ---------------------------------------------------------------------------
//  tests/middleware: not-found.js
// ---------------------------------------------------------------------------

    import test from 'ava';
    import notFoundMiddleware from 'app/middleware/not-found';

    test('should set error code', async (assert) => {
        try {
            await notFoundMiddleware({}, async () => {
                // ... do nothing
            });
        } catch (error) {
            assert.is(error.code, 'not_found');
        }
    });

    test('should set error message', async (assert) => {
        try {
            await notFoundMiddleware({}, async () => {
                // ... do nothing
            });
        } catch (error) {
            assert.is(error.message, 'Not Found (not_found)');
        }
    });

    test('should set error status', async (assert) => {
        try {
            await notFoundMiddleware({}, async () => {
                // ... do nothing
            });
        } catch (error) {
            assert.is(error.status, 404);
        }
    });
