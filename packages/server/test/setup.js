// ---------------------------------------------------------------------------
//  test: setup.js
// ---------------------------------------------------------------------------

    process.env.LOGGING_ENABLED = 'false';
    process.env.NODE_PATH = require('path').resolve(__dirname, '../');
    require('module').Module._initPaths();

    require('babel-polyfill');
    require('babel-register');
