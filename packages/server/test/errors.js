// ---------------------------------------------------------------------------
//  test: errors.js
// ---------------------------------------------------------------------------

    import test from 'ava';

    import {
        newError,
        errors
    } from 'app/errors';

    test('message get a colon appended when no code is set', (assert) => {
        const _error = newError('My error')(null, 'test');
        assert.is(_error.message, 'My error: test');
    });

    test('code get a colon appended when data is set', (assert) => {
        const _error = newError('My error', {type: 'my_code'})(null, 'test');
        assert.is(_error.message, 'My error (my_code): test');
    });

    test('separate error data with spaces when more are provided', (assert) => {
        const _error = newError('My error')(null, 'test', 'something');
        assert.is(_error.message, 'My error: test something');
    });

    test.serial('log errors marked for logging', () => {
        newError('Logged error', {log: true})();
    });

    test('missing_required_parameters with no data', (assert) => {
        const _error = errors.missing_required_parameters();

        assert.is(_error.code, 'missing_required_parameters');
        assert.is(_error.status, 400);
        assert.is(_error.message, 'Missing Required Parameters (missing_required_parameters)');
        assert.is(_error.data.length, 0);
    });

    test('missing_required_parameters with data', (assert) => {
        const _error = errors.missing_required_parameters(null, 'email');

        assert.is(_error.code, 'missing_required_parameters');
        assert.is(_error.status, 400);
        assert.is(_error.message, 'Missing Required Parameters (missing_required_parameters): email');
        assert.is(_error.data[0], 'email');
    });

    test('missing_required_parameters with custom code', (assert) => {
        const _error = errors.missing_required_parameters('missing_email', 'email');

        assert.is(_error.code, 'missing_email');
        assert.is(_error.type, 'missing_required_parameters');
        assert.is(_error.status, 400);
        assert.is(_error.message, 'Missing Required Parameters (missing_email): email');
        assert.is(_error.data[0], 'email');
    });

    test('not_found with no data', (assert) => {
        const _error = errors.not_found();

        assert.is(_error.code, 'not_found');
        assert.is(_error.status, 404);
        assert.is(_error.message, 'Not Found (not_found)');
    });

    test('method_not_allowed with no data', (assert) => {
        const _error = errors.method_not_allowed();

        assert.is(_error.code, 'method_not_allowed');
        assert.is(_error.status, 405);
        assert.is(_error.message, 'Method Not Allowed (method_not_allowed)');
    });

    test('type_error with no data', (assert) => {
        const _error = errors.type_error();

        assert.is(_error.code, 'type_error');
        assert.is(_error.status, 400);
        assert.is(_error.message, 'Type Error (type_error)');
    });
