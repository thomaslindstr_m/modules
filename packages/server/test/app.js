// ---------------------------------------------------------------------------
//  test: app.js
// ---------------------------------------------------------------------------

    import test from 'ava';
    import chalk from 'chalk';

    import isObject from '@amphibian/is-object';
    import isFunction from '@amphibian/is-function';

    import createApp from 'app';

    import {
        isLoggingEnabled,
        setLoggingEnabled
    } from 'app/env';

    test.serial('createApp should return an object', (assert) => {
        const _app = createApp();
        assert.true(isObject(_app));
        _app.listener.close();
    });

    test.serial('env should be an object', (assert) => {
        const _app = createApp();
        assert.true(isObject(_app.env));
        _app.listener.close();
    });

    test.serial('app should be an object', (assert) => {
        const _app = createApp();
        assert.true(isObject(_app.app));
        _app.listener.close();
    });

    test.serial('listener should be an object', (assert) => {
        const _app = createApp();
        assert.true(isObject(_app.listener));
        _app.listener.close();
    });

    test.serial('router should be a function', (assert) => {
        const _app = createApp();
        assert.true(isFunction(_app.router));
        _app.listener.close();
    });

    test.serial('registerHandler should be a function', (assert) => {
        const _app = createApp();
        assert.true(isFunction(_app.registerHandler));
        _app.listener.close();
    });

    test.serial('allow custom options.port', (assert) => {
        const _app = createApp({port: 5000});
        assert.is(_app.listener.address().port, 5000);
        _app.listener.close();
    });

    test.serial.cb('should log when starting up', (assert) => {
        assert.plan(1);

        const _wasLoggingEnabled = isLoggingEnabled;
        setLoggingEnabled(true);

        const _app = createApp();

        _app.app.on('log', (...data) => {
            assert.end(assert.is(data[1], chalk.green('Now listening')));
            setLoggingEnabled(_wasLoggingEnabled);
            _app.listener.close();
        });
    });

    test.serial.cb('should log when an error occurs', (assert) => {
        assert.plan(1);

        const _wasLoggingEnabled = isLoggingEnabled;
        setLoggingEnabled(true);

        const _app = createApp();

        _app.app.on('log', (...data) => {
            assert.end(assert.is(data[0], chalk.red('[Error]')));
            setLoggingEnabled(_wasLoggingEnabled);
            _app.listener.close();
        });

        _app.app.emit('error', new Error('hello'));
    });
