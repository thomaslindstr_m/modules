// ---------------------------------------------------------------------------
//  is-function
//
//  function type checker
//  - variable: Variable to be checked
// ---------------------------------------------------------------------------

    var STRING_FUNCTION = 'function';

    function isFunction(variable) {
        return typeof variable === STRING_FUNCTION;
    }

    module.exports = isFunction;
