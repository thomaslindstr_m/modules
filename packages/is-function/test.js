// ---------------------------------------------------------------------------
//  test.js
// ---------------------------------------------------------------------------

    var isFunction = require('./index.js');
    var assert = require('assert');

    describe('is-function', function() {
        it('should return true for function', function () {
            assert.equal(isFunction(function () {}), true);
        });

        it('should return false for objects', function () {
            assert.equal(isFunction({}), false);
        });

        it('should return false for strings', function () {
            assert.equal(isFunction(''), false);
        });

        it('should return false for arrays', function () {
            assert.equal(isFunction([]), false);
        });

        it('should return false for undefined', function () {
            assert.equal(isFunction(undefined), false);
        });

        it('should return false for null', function () {
            assert.equal(isFunction(null), false);
        });

        it('should return false for false', function () {
            assert.equal(isFunction(false), false);
        });

        it('should return false for true', function () {
            assert.equal(isFunction(true), false);
        });
    });
