# is-function

[![build status](https://gitlab.com/ci/projects/31209/status.png?ref=master)](https://gitlab.com/ci/projects/31209?ref=master)

function type checker

```
npm install @amphibian/is-function
```

```javascript
var isFunction = require('@amphibian/is-function');

isFunction(function () {}); // > true
isFunction(true); // > false
```
