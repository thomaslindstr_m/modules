// ---------------------------------------------------------------------------
//  test.js
// ---------------------------------------------------------------------------

    var writeCookie = require('./index.js');
    var assert = require('assert');

    describe('write-cookie', function () {
        it('should write a cookie', function () {
            global.window = {
                encodeURIComponent: encodeURIComponent,
                location: {
                    hostname: 'bazinga.domain.com'
                },
                document: {
                    cookie: ''
                }
            };

            var _expirationDate = new Date();
            _expirationDate.setYear(new Date().getFullYear() + 1);

            writeCookie('test_cookie', 'test_value');

            assert.equal(window.document.cookie, [
                'test_cookie=test_value',
                'expires=' + _expirationDate.toUTCString(),
                'domain=domain.com',
                'path=/'
            ].join(';'));
        });

        it('should allow overrides to expiration date', function () {
            global.window = {
                encodeURIComponent: encodeURIComponent,
                location: {
                    hostname: 'bazinga.domain.com'
                },
                document: {
                    cookie: ''
                }
            };

            var _expirationDate = new Date();
            _expirationDate.setYear(new Date().getFullYear() + 5);

            writeCookie('test_cookie', 'test_value', {
                expiration: _expirationDate
            });

            assert.equal(window.document.cookie, [
                'test_cookie=test_value',
                'expires=' + _expirationDate.toUTCString(),
                'domain=domain.com',
                'path=/'
            ].join(';'));
        });

        it('should allow overrides to domain', function () {
            global.window = {
                encodeURIComponent: encodeURIComponent,
                location: {
                    hostname: 'bazinga.domain.com'
                },
                document: {
                    cookie: ''
                }
            };

            var _expirationDate = new Date();
            _expirationDate.setYear(new Date().getFullYear() + 1);

            writeCookie('test_cookie', 'test_value', {
                domain: 'custom-domain.com'
            });

            assert.equal(window.document.cookie, [
                'test_cookie=test_value',
                'expires=' + _expirationDate.toUTCString(),
                'domain=custom-domain.com',
                'path=/'
            ].join(';'));
        });

        it('should allow overrides to path', function () {
            global.window = {
                encodeURIComponent: encodeURIComponent,
                location: {
                    hostname: 'bazinga.domain.com'
                },
                document: {
                    cookie: ''
                }
            };

            var _expirationDate = new Date();
            _expirationDate.setYear(new Date().getFullYear() + 1);

            writeCookie('test_cookie', 'test_value', {
                path: '/subfolder'
            });

            assert.equal(window.document.cookie, [
                'test_cookie=test_value',
                'expires=' + _expirationDate.toUTCString(),
                'domain=domain.com',
                'path=/subfolder'
            ].join(';'));
        });

        it('should encode values', function () {
            global.window = {
                encodeURIComponent: encodeURIComponent,
                location: {
                    hostname: 'bazinga.domain.com'
                },
                document: {
                    cookie: ''
                }
            };

            var _expirationDate = new Date();
            _expirationDate.setYear(new Date().getFullYear() + 1);

            writeCookie('test_cookie', 'test_value$for%!=');

            assert.equal(window.document.cookie, [
                'test_cookie=test_value%24for%25!%3D',
                'expires=' + _expirationDate.toUTCString(),
                'domain=domain.com',
                'path=/'
            ].join(';'));
        });
    });
