// ---------------------------------------------------------------------------
//  write-cookie.js
// ---------------------------------------------------------------------------

    var STRING_EQUALS = '=';
    var STRING_DOT = '.';
    var STRING_SEMICOLON = ';';
    var STRING_SLASH = '/';

    var STRING_EXPIRES = 'expires';
    var STRING_DOMAIN = 'domain';
    var STRING_PATH = 'path';

    var plusOne = +1;
    var minusTwo = -2;

    /**
     * Write cookie
     * @param {string} name
     * @param {string} value
     * @param {object} options
     * @param {date} options.expiration
     * @param {string} options.domain
     * @param {string} options.path
     *
     * @example writeCookie('my_cookie', 'hello', {domain: 'www.mydomain.com'})
    **/
    function writeCookie(name, value, options) {
        var _options = options || {};

        var _cookie = [];
        _cookie.push(name + STRING_EQUALS + window.encodeURIComponent(value));

        var _expiration = null;

        if (_options.expiration
        && (_options.expiration instanceof Date)) {
            _expiration = _options.expiration;
        } else {
            _expiration = new Date();
            _expiration.setYear(new Date().getFullYear() + plusOne);
        }

        _cookie.push(STRING_EXPIRES + STRING_EQUALS + _expiration.toUTCString());

        var _domain = null;

        if (_options.domain) {
            _domain = _options.domain;
        } else {
            _domain = window.location.hostname.split(STRING_DOT).slice(minusTwo).join(STRING_DOT);
        }

        _cookie.push(STRING_DOMAIN + STRING_EQUALS + _domain);

        var _path = null;

        if (_options.path) {
            _path = _options.path;
        } else {
            _path = STRING_SLASH;
        }

        _cookie.push(STRING_PATH + STRING_EQUALS + _path);
        window.document.cookie = _cookie.join(STRING_SEMICOLON);
    }

    module.exports = writeCookie;
