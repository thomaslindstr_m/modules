# write-cookie

[![build status](https://gitlab.com/thomaslindstr_m/write-cookie/badges/master/build.svg)](https://gitlab.com/thomaslindstr_m/write-cookie/commits/master)

write browser cookie

```
npm install @amphibian/write-cookie
```

## Arguments

### `name` **String**

Cookie name.

### `value` **String**

Cookie value. Will be URL encoded.

### `options` **Object**

Options for setting the cookie.

- `expiration` **Date**: Cookie expiration date. 1 year from *now* is the default.
- `domain` **String**: Cookie domain. Current hostname without subdomains is the default, eg. `npmjs.org`.
- `path` **String**: Cookie path. `/` is the default.


## Example

```javascript
var writeCookie = require('@amphibian/write-cookie');

// By default, cookies live for a year.
writeCookie('test_cookie', 'test_value');

// This can be overridden using `options`
writeCookie('test_cookie2', 'test_value2', {
    expiration: new Date((new Date()).getTime() + (1000 * 60 * 60)) // 1 hour
});

// By default, the cookie domain is set to the current hostname without
// subdomains included. Override this using the `domains` option
writeCookie('test_cookie3', 'test_value3', {
    domains: 'my-domain.com'
});
```
