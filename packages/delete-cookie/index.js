// ---------------------------------------------------------------------------
//  delete-cookie.js
// ---------------------------------------------------------------------------

    var EMPTY_STRING = require('empty-string');
    var writeCookie = require('@amphibian/write-cookie');

    /**
     * Delete cookie
     * @param {string} name
     * @param {object} options
     * @param {string} options.domain
     * @param {string} options.path
     *
     * @example deleteCookie('my_cookie', {domain: 'www.mydomain.com'})
    **/
    function deleteCookie(name, options = {}) {
        writeCookie(name, EMPTY_STRING, {
            expiration: new Date('Thu, 01 Jan 1970 00:00:01 GMT'),
            domain: options.domain,
            path: options.path
        });
    }

    module.exports = deleteCookie;
