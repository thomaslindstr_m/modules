// ---------------------------------------------------------------------------
//  test.js
// ---------------------------------------------------------------------------

    var deleteCookie = require('./index.js');
    var assert = require('assert');

    describe('delete-cookie', function () {
        it('should delete a cookie', function () {
            global.window = {
                encodeURIComponent: encodeURIComponent,
                location: {
                    hostname: 'bazinga.domain.com'
                },
                document: {
                    cookie: ''
                }
            };

            var _expirationDate = new Date('Thu, 01 Jan 1970 00:00:01 GMT');
            deleteCookie('test_cookie');

            assert.equal(window.document.cookie, [
                'test_cookie=',
                'expires=' + _expirationDate.toUTCString(),
                'domain=domain.com',
                'path=/'
            ].join(';'));
        });

        it('should allow overrides to domain', function () {
            global.window = {
                encodeURIComponent: encodeURIComponent,
                location: {
                    hostname: 'bazinga.domain.com'
                },
                document: {
                    cookie: ''
                }
            };

            var _expirationDate = new Date('Thu, 01 Jan 1970 00:00:01 GMT');

            deleteCookie('test_cookie', {
                domain: 'custom-domain.com'
            });

            assert.equal(window.document.cookie, [
                'test_cookie=',
                'expires=' + _expirationDate.toUTCString(),
                'domain=custom-domain.com',
                'path=/'
            ].join(';'));
        });

        it('should allow overrides to path', function () {
            global.window = {
                encodeURIComponent: encodeURIComponent,
                location: {
                    hostname: 'bazinga.domain.com'
                },
                document: {
                    cookie: ''
                }
            };

            var _expirationDate = new Date('Thu, 01 Jan 1970 00:00:01 GMT');

            deleteCookie('test_cookie', {
                path: '/subfolder'
            });

            assert.equal(window.document.cookie, [
                'test_cookie=',
                'expires=' + _expirationDate.toUTCString(),
                'domain=domain.com',
                'path=/subfolder'
            ].join(';'));
        });
    });
