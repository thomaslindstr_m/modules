# delete-cookie

[![build status](https://gitlab.com/thomaslindstr_m/delete-cookie/badges/master/build.svg)](https://gitlab.com/thomaslindstr_m/delete-cookie/commits/master)

delete browser cookie

```
npm install @amphibian/delete-cookie
```

## Arguments

### `name` **String**

Cookie name.

### `options` **Object**

Options for setting the cookie.

- `domain` **String**: Cookie domain. Current hostname without subdomains is the default, eg. `npmjs.org`.
- `path` **String**: Cookie path. `/` is the default.


## Example

```javascript
var deleteCookie = require('@amphibian/delete-cookie');

deleteCookie('test_cookie');

// By default, the cookie domain is set to the current hostname without
// subdomains included. Override this using the `domains` option
deleteCookie('test_cookie2', {
    domains: 'my-domain.com'
});
```
