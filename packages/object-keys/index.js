// ---------------------------------------------------------------------------
//  object-keys
//  return keys of an object
// ---------------------------------------------------------------------------

    var objectHasProperty = require('@amphibian/object-has-property');
    var hasObjectKeys = objectHasProperty(Object, 'keys');
    var objectKeysFunction = Object.keys;

    function objectKeys(object) {
        if (hasObjectKeys) {
            return objectKeysFunction(object);
        } else {
            var _keys = [];

            for (_key in object) {
                if (objectHasProperty(object, _key)) {
                    _keys.push(_key);
                }
            }

            return _keys;
        }
    }

    module.exports = objectKeys;
