# object-keys

return keys of an object

```
npm install @amphibian/object-keys
```

```javascript
var objectKeys = require('@amphibian/object-keys');
var object = {
    foo: 'bar',
    bar: 'foo'
};

console.log(objectKeys(object)); // > ['foo', 'bar']
```
