// ---------------------------------------------------------------------------
//  test.js
// ---------------------------------------------------------------------------

    var removeFromArray = require('./index.js');
    var assert = require('assert');

    describe('in-array', function() {
        var mammals = ['cat', 'bird', 'dog', 'horse', 'fish'];

        it('should remove an index from an array', function () {
            removeFromArray('fish', mammals);

            assert.equal(mammals[0], 'cat');
            assert.equal(mammals[1], 'bird');
            assert.equal(mammals[2], 'dog');
            assert.equal(mammals[3], 'horse');
            assert.equal(mammals.length, 4);
        });

        it('should fail silently when the index is not present in array', function () {
            removeFromArray('dragon', mammals);
            assert.equal(mammals.length, 4);
        });

        var array = ['test'];
        var parentArray = [array, 'something else'];

        it('should be able to remove arrays from an array', function () {
            removeFromArray(array, parentArray);

            assert.equal(parentArray[0], 'something else');
            assert.equal(parentArray.length, 1);
        });

        it('should return the updated array', function () {
            var newArray = removeFromArray('cat', mammals);
            assert.equal(newArray, mammals);
        });
    });
