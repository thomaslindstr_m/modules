// ---------------------------------------------------------------------------
//  remove-from-array
//
//  remove an index from an array
//  - needle: The index to remove
//  - array {Array}: The array to remove index from
// ---------------------------------------------------------------------------

    var iterateDownArray = require('@amphibian/iterate-down-array');

    function removeFromArray(needle, array) {
        iterateDownArray(array, function (index, i, end) {
            if (needle === index) {
                array.splice(i, 1);
                end();
            }
        });

        return array;
    }

    module.exports = removeFromArray;
