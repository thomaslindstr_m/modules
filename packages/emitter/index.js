// ---------------------------------------------------------------------------
//  emitter
//  small utility emitter library
// ---------------------------------------------------------------------------

    var isString = require('@amphibian/is-string');
    var isArray = require('@amphibian/is-array');
    var isFunction = require('@amphibian/is-function');
    var iterateUpArray = require('@amphibian/iterate-up-array');
    var iterateDownArray = require('@amphibian/iterate-down-array');
    var objectHasProperty = require('@amphibian/object-has-property');

    var ERROR_EVENT_MUST_BE_STRING = 'emitter: `event` must be a String';
    var ERROR_CALLBACK_MUST_BE_FUNCTION = 'emitter: `callback` must be a Function';

    var slice = [].slice;

    var emitter = {
        bindings: {}, // {listeners: [], propagationStopped: false}

        /**
         * Emit an event
         * @param {string} event
        **/
        emit: function (event) {
            if (!event || !isString(event)) {
                throw new Error(ERROR_EVENT_MUST_BE_STRING);
            }

            var _bindings = this.bindings[event];
            var _arguments = slice.call(arguments, 1);

            if (_bindings) {
                _bindings.propagationStopped = false;

                iterateUpArray(_bindings.listeners, function (binding) {
                    if (!binding
                    || (_bindings.propagationStopped)) {
                        return;
                    }

                    binding.apply(undefined, _arguments);
                });
            }
        },

        /**
         * Listen for an event or multiple events
         * @param {string|array} event
         * @param {function} callback
         *
         * @return {function} unsubscribe
        **/
        on: function (event, callback) {
            if (isArray(event)) {
                iterateUpArray(event, function (singleEvent) {
                    emitter.on(singleEvent, callback);
                });

                return;
            }

            if (!event || !isString(event)) {
                throw new Error(ERROR_EVENT_MUST_BE_STRING);
            } else if (!callback || !isFunction(callback)) {
                throw new Error(ERROR_CALLBACK_MUST_BE_FUNCTION)
            }

            if (!objectHasProperty(this.bindings, event)) {
                this.bindings[event] = {
                    listeners: [],
                    propagationStopped: false
                };
            }

            this.bindings[event].listeners.push(callback);
            return this.off.bind(this, event, callback);
        },

        /**
         * Unsubscribe a listener
         * @param {string} event
         * @param {function} callback
        **/
        off: function (event, callback) {
            if (!event || !isString(event)) {
                throw new Error(ERROR_EVENT_MUST_BE_STRING);
            } else if (!callback || !isFunction(callback)) {
                throw new Error(ERROR_CALLBACK_MUST_BE_FUNCTION)
            }

            var _bindings = this.bindings[event];

            if (_bindings) {
                iterateDownArray(_bindings.listeners, function (binding, i) {
                    if (callback === binding) {
                        _bindings.listeners.splice(i, 1);
                    }
                });
            }
        },

        /**
         * Stop propagation of an event
         * @param {string} event
        **/
        stopPropagation: function (event) {
            if (!event || !isString(event)) {
                throw new Error(ERROR_EVENT_MUST_BE_STRING);
            }

            var _bindings = this.bindings[event];
            _bindings.propagationStopped = true;
        }
    };

    module.exports = emitter;
