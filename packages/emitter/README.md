# emitter

small utility emitter library

```
npm install @amphibian/emitter
```

```javascript
var emitter = require('@amphibian/emitter');
var unsubscribe = emitter.on('some-event', function (data) {
    console.log(data);
});

emitter.on(['some-event', 'other-event'], function (data) {
    console.log('I get called for both of the above.');
    emitter.stopPropagation('some-event');
});

emitter.on('some-event', function () {
    console.log('I will not get called. The above listener stopped propagation.');
});

setTimeout(function () {
    emitter.emit('some-event', {content: 'something'});
    unsubscribe();

    // After unsubscribing, nothing happens when emitting the event
    emitter.emit('some-event', {content: 'something'});
}, 100);
```

## Best practices

Relying on the exact contents of a string is error prone. Instead, consider creating utility functions for subscribing and unsubscribing to events.

```javascript
var emitter = require('@amphibian/emitter');

function onSomeEvent(callback) {
    return emitter.on('some-event', callback);
}

onSomeEvent(function () {
    console.log('hello');
});

emitter.emit('some-event');
```
