// ---------------------------------------------------------------------------
//  test.js
// ---------------------------------------------------------------------------

    var emitter = require('./index.js');
    var assert = require('assert');

    describe('emitter', function () {
        it('should emit events', function (done) {
            emitter.on('emit', () => {
                done();
            });

            emitter.emit('emit');
        });

        it('should emit events with data', function (done) {
            var _testData = {hello: true};
            var _moreData = {more: 'hello'};

            emitter.on('emit-2', (data, moreData) => {
                assert.deepEqual(_testData, data);
                assert.deepEqual(_moreData, moreData);
                done();
            });

            emitter.emit('emit-2', _testData, _moreData);
        });

        it('should emit multi-events with data', function (done) {
            var _testData = {hello: true};

            emitter.on(['emit-3-1', 'emit-3'], (data) => {
                assert.deepEqual(_testData, data);
                done();
            });

            emitter.emit('emit-3', _testData);
        });

        it('should unsubscribe listeners with the returned function', function () {
            var _didCall = false;

            var unsubscribe = emitter.on('emit-4', () => {
                _didCall = true;
            });

            unsubscribe();
            emitter.emit('emit-4');
            assert.equal(_didCall, false);
        });

        it('should unsubscribe listeners through the emitter api', function () {
            var _didCall = false;

            function listenerFunction() {
                _didCall = true;
            }

            emitter.on('emit-5', listenerFunction);
            emitter.off('emit-5', listenerFunction);
            emitter.emit('emit-5');

            assert.equal(_didCall, false);
        });

        it('should not allow emitting an event whose name is not a string', function () {
            try {
                emitter.emit([]);
            } catch (error) {
                assert.equal(error.message, 'emitter: `event` must be a String');
                return;
            }

            throw new Error('no error was thrown');
        });

        it('should not allow listening on an event whose name is not a string', function () {
            try {
                emitter.on({});
            } catch (error) {
                assert.equal(error.message, 'emitter: `event` must be a String');
                return;
            }

            throw new Error('no error was thrown');
        });

        it('should not allow adding a listener whose callback is not a function', function () {
            try {
                emitter.on('event', []);
            } catch (error) {
                assert.equal(error.message, 'emitter: `callback` must be a Function');
                return;
            }

            throw new Error('no error was thrown');
        });

        it('should not allow unsubscribing an event whose name is not a string', function () {
            try {
                emitter.off([]);
            } catch (error) {
                assert.equal(error.message, 'emitter: `event` must be a String');
                return;
            }

            throw new Error('no error was thrown');
        });

        it('should not allow unsubscribing a listener whose callback is not a function', function () {
            try {
                emitter.on('event', []);
            } catch (error) {
                assert.equal(error.message, 'emitter: `callback` must be a Function');
                return;
            }

            throw new Error('no error was thrown');
        });

        it('should allow the stopping of propagation', function () {
            var _didCall = false;

            emitter.on('emit-6', function () {
                emitter.stopPropagation('emit-6');
            });

            emitter.on('emit-6', function () {
                _didCall = true;
            });

            emitter.emit('emit-6');
            assert.equal(_didCall, false);
        });
    });
